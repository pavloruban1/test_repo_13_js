const person = {
    name: 'John',
    age: 30,
    gender: 'male'
};

const newPerson = Object.assign({}, person);
newPerson.age = 35;

console.log(person);
console.log(newPerson);


const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
};
function printCarInfo(car) {
    if (car.year < 2001) {
        console.log("Машина занадто стара");
    } else{
        console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}`);
    }
}
printCarInfo(car);


const str = 'JavaScript is a high-level programming language';
function countWords(str) {
    const wordsArr = str.match(/\b\w+\b/g);
    return wordsArr.length;
}
console.log(countWords(str));


const rts = 'JavaScript is awesome!';
function reverseString(rts) {
    return rts.split('').reverse().join('');
}
console.log(reverseString(rts));


const qwe = 'JavaScript is awesome!';

function reverseWordsInString(qwe) {
    const wordsArray = qwe.split(' ');
    const reversedWordsArray = wordsArray.map(word => {
        return word.split('').reverse().join('');
    });
    return reversedWordsArray.join(' ');
}
console.log(reverseWordsInString(qwe));








